using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AddSceneManager : MonoBehaviour
{
    public GameObject menu;
    public GameObject loadingInterface;
    public Image loadingProgressBar;

    private List<AsyncOperation> scenesToLoad = new List<AsyncOperation>();
    public void StartGame()
    {
        HideMenu();
        ShowLoadingScreen();
        scenesToLoad.Add(SceneManager.LoadSceneAsync("city update"));
        SceneManager.LoadScene("techScene", LoadSceneMode.Additive);
        StartCoroutine(LoadingScreen());

    }

    public void HideMenu()
    {
        menu.SetActive(false);
    }

    public void ShowLoadingScreen()
    {
       loadingInterface.SetActive(true);
        
    }

    IEnumerator LoadingScreen()
    {
        float totalProgress = 0;
        for(int i=0; i<scenesToLoad.Count; ++i)
        {
            while (!scenesToLoad[i].isDone)
            {

                totalProgress += scenesToLoad[i].progress;
                loadingProgressBar.fillAmount = totalProgress / scenesToLoad.Count;
                yield return null;
            }
        }
    }
   // void Update()
   // {
    //    if (Input.GetKeyDown(KeyCode.P))
   //     {
   //         SceneManager.LoadScene(1, LoadSceneMode.Additive);
   //     }

  //  }
}
