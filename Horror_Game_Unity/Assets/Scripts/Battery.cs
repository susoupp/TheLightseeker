using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Battery : MonoBehaviour, ICollectible
{
   //don't need update method, repetitive method to run when it's necessary
   //instead of defining a delegate and creating an instance of it, you can, instead, create an action that will work in the same way.
   public static event HandleBatteryCollector OnBatteryCollected;
  //new observer, function containers,store and call a function as if it were a variable
  //One of the main benefits of using a delegate, is that you can change the function that is triggered when the delegate is called.
   public delegate void HandleBatteryCollector(ItemData itemData);

   public ItemData batData;
   private BatteryLive batteryLife;
  
   
   public void Collect()
   {
      Debug.Log("Battery, yea!");
      Destroy(gameObject);
     OnBatteryCollected?.Invoke(batData);
   }

   

   
}
