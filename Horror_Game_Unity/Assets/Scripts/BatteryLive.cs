 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryLive : MonoBehaviour
{
    public Image batteryImage; // Reference to the Image for the UI battery bar
    public CanvasGroup uiCanvasGroup; // Reference to the CanvasGroup of the UI element
    public float batteryLife = 10f;
    public float batteryDrainRate = 1f;
    public float lowBatteryThreshold = 3f; // Set the threshold for low battery

    public bool isFlashlightOn = false;
    private Flashlight flashlightScript; // Reference to the Flashlight script

    void Start()
    {
        flashlightScript = FindFirstObjectByType<Flashlight>(); // Find the Flashlight script in the scene
        UpdateBatteryUI();
    }

    void Update()
    {  
        CheckBatteryStatus();
        if (isFlashlightOn)
        {
            DrainBattery();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            ToggleFlashlight();
        }
        UpdateBatteryUI();
    }

    void ToggleFlashlight()
    {
        isFlashlightOn = !isFlashlightOn;

        UpdateBatteryUI();

        // Call the ToggleFlashlight method from the Flashlight script
        if (flashlightScript != null)
        {
            flashlightScript.ToggleFlashlight(isFlashlightOn);
        }

        // Check if battery is low and adjust the visibility of the UI element
       // CheckBatteryStatus();
    }

    void DrainBattery()
    {
        batteryLife -= Time.deltaTime * batteryDrainRate;

        if (batteryLife <= 0f)
        {
            isFlashlightOn = false;
            batteryLife = 0f;

            // Turn off the flashlight when the battery runs out
            if (flashlightScript != null)
            {
                flashlightScript.ToggleFlashlight(false);
            }
        }

        UpdateBatteryUI();

        // Check if battery is low and adjust the visibility of the UI element
        CheckBatteryStatus();
    }

    void UpdateBatteryUI()
    {
        if (batteryImage != null)
        {
            // Adjust the fill amount of the Image based on the battery life
            batteryImage.fillAmount = batteryLife / 10f; // Normalize to the range [0, 1]
        }
    }

    void CheckBatteryStatus()
    {
        // If battery is below the threshold, make the UI element invisible
        if (batteryLife <= lowBatteryThreshold)
        {
            SetUIVisibility(false);
        }
        else
        {
            SetUIVisibility(true);
        }
    }

    void SetUIVisibility(bool isVisible)
    {
        if (uiCanvasGroup != null)
        {
            uiCanvasGroup.alpha = isVisible ? 1f : 0f; // Set alpha to 1 for visible, 0 for invisible
            uiCanvasGroup.blocksRaycasts = isVisible; // Disable interaction when invisible
        }
    }
}
