using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Vector3 _offset = new Vector3(1f, 6f, -26f);
    [SerializeField]private float _smoothTime = 0.25f;
    private Vector3 _velocity = Vector3.zero;
 
   
   

   [SerializeField] private Transform target;

    private void Update()
    {
        Vector3 targetPosition = target.position + _offset;
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref _velocity, _smoothTime);
       
    }
  
    

}
