using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Camera mouseCamera;
    public GameObject lookAtMouse;
   public void Update()
   {
       Vector3 mousePosition = new Vector3();
       mousePosition = mouseCamera.ScreenToWorldPoint(Input.mousePosition);
       mousePosition.z = Camera.main.nearClipPlane;
       transform.position = mousePosition;
   }
}
