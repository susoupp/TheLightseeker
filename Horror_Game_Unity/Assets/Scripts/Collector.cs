using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{
    //when collided  Both GameObjects must contain a Collider component. One must have Collider.isTrigger enabled, and contain a Rigidbody
    private void OnTriggerEnter(Collider collision)
    {
        ICollectible collectible = collision.GetComponent<ICollectible>();
        if(collectible !=null)
        {
            collectible.Collect();
        }
    }
}
