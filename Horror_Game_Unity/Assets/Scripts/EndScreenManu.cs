using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreenManu : MonoBehaviour
{
    public void PlayAgain()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("city update");
        SceneManager.LoadScene("techScene", LoadSceneMode.Additive);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
