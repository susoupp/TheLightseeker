using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Enemies: MonoBehaviour
{
    private NavMeshAgent _enemy;
    public Transform PlayerTarget;
    public GameObject cat;
    
    
    void Awake()
    {
        _enemy = GetComponent<NavMeshAgent>();
        
    }

    private void Update()
    {
        _enemy.SetDestination(PlayerTarget.position);
      
    }

    

}
