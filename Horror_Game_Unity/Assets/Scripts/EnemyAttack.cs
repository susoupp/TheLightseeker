using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class EnemyAttack : MonoBehaviour
{
   
    //public Image healthBar;
    public HealthManager Health;
    public GameEvent PlayerHealthChange;
   public PlayerStats healthAmount ;
    


    private void Update()
    {
       
    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            Debug.Log("hurt");
            Hurt();
            
        }
        informUI();
       
    }
   public void Hurt()
    {
        Health.TakeDamage(15f);
        
    }

   public void informUI()
   {
       PlayerHealthChange.Raise(this, healthAmount);
   }

 

   
    
}
