using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;

using UnityEngine;

public class Flashlight : MonoBehaviour
{
    private Light flashlight;
    //public bool isLighting;
    private bool isLightOn;

    void Awake()
    {
        flashlight = GetComponent<Light>();
        flashlight.enabled = false;
        isLightOn = false;
    }

   
    public void ToggleFlashlight(bool isOn)
    {
        isLightOn = isOn;
        flashlight.enabled = isLightOn;
    }




}
