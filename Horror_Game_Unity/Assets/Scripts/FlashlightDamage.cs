using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightDamage : MonoBehaviour
{
    public Transform attackPoint;
    public LayerMask enemyLayers;
    public Vector3 hitBoxSize = Vector3.one;
    public float minAngle;
    public float maxAngle;
    public float maxDistance;
    private Transform PlayerTransform;
    private BatteryLive batteryScript;

    private void Start()
    {
        PlayerTransform = GameObject.Find("Player").transform;
    }


    void Update()
    {
           
           Vector3 playerPosition = PlayerTransform.position;
           
           Collider[] colliders = Physics.OverlapBox(playerPosition * maxDistance * .5f, Vector3.one * maxDistance, Quaternion.identity,  enemyLayers);
           Debug.Log(colliders);
           foreach(Collider collider in colliders)
           {
               
               Vector3 targetDir = collider.transform.position - transform.position;
               float angle = Vector3.Angle(targetDir, transform.forward);
               float dist = Vector3.Distance(collider.transform.position, transform.position);
               Debug.Log($"angle:{angle}");
               if (angle < minAngle && angle > maxDistance && dist < maxDistance && batteryScript.isFlashlightOn == true)
               {
                   Debug.Log($"We hit {collider.name}");
                   if (collider.TryGetComponent<Enemies>(out Enemies enemy))
                   {
                       
                       //deal dmgto enemy   
                   }
               }
               
           }
    }

    

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, maxDistance);
        Gizmos.DrawLine(transform.position, (Quaternion.AngleAxis(minAngle, Vector3.up) * transform.forward).normalized * maxDistance + transform.position);
        Gizmos.DrawLine(transform.position, (Quaternion.AngleAxis(maxAngle, Vector3.up) * transform.forward).normalized * maxDistance + transform.position);
        Gizmos.DrawWireCube(transform.position + transform.forward * maxDistance* 0.5f, Vector3.one * maxDistance );   

    }


    // public void informEnemy()
   // {
   //    EnemyHealth.Raise(this, healthAmount);
   // }
}
