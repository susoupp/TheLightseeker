using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;
using UnityEngine.SceneManagement;


public class HealthManager : MonoBehaviour
{
   public Image healthBar;
   public PlayerStats healthAmount ;
   //public PlayerStats Health;
   public GameObject player;
   

   public void Awake()
   {
      healthAmount.value = 100f;
   }

   void Update()
   {
      
      if ( healthAmount.value <= 0)
      {
         RestartGame();
      }
   }

   public void TakeDamage(float damage)
   {
      healthAmount.value -= damage;
   }

   public void Heal(float healingAmount)
   {
      healthAmount.value += healingAmount;
     // healthAmount = Mathf.Clamp(healthAmount, 0, 100);


   }
   void RestartGame() 
   { 
      int currentSceneIndex = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex; 
      UnityEngine.SceneManagement.SceneManager.LoadScene("city update");
      SceneManager.LoadScene("techScene", LoadSceneMode.Additive);
     
   }
}
