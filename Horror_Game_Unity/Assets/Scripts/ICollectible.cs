using System.Collections;
using System.Collections.Generic;
using UnityEngine;

////Interfaces allow scripts to interact with each other based on the functionality that they implement, not what they are.
public interface ICollectible
{
    //when using ICollectible interface has to have this method
    public void Collect();
}
