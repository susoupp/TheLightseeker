using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    //list of objects in inventory and adding new
    public List<InventoryItem> inventory = new List<InventoryItem>();
    //dictionary for items to stack and adding new 
    private Dictionary<ItemData, InventoryItem> itemDictionary= new Dictionary<ItemData, InventoryItem>();

    // is called when the object becomes enabled and active.
    private void OnEnable()
    {
        Battery.OnBatteryCollected += Add;
        Notes.OnNoteCollected += Add;
    }
    
    //is called when the behaviour becomes disabled.
    private void OnDisable()
    {
        Battery.OnBatteryCollected -= Add;
        Notes.OnNoteCollected -= Add;
    }
    public void Add(ItemData itemData)
    {
        //is trying to get value with specific key itemData and value is InventoryItem
        if(itemDictionary.TryGetValue(itemData, out InventoryItem item))
            //true adding to stack new item
        {
            item.AddtoStack();
            Debug.Log($"{item.itemData.displayName}total stack is now{item.stackSize}");
        }
        else
        { //false creating item on dictionary and new value is created and adding to inventory
            InventoryItem newItem = new InventoryItem(itemData);
            inventory.Add(newItem);
            itemDictionary.Add(itemData, newItem);
            Debug.Log($"{itemData.displayName}added to inventory first time");
        }
    }
    public void Remove(ItemData itemData)
    {
        //if value is true is removing one from stack or whole
        if(itemDictionary.TryGetValue(itemData, out InventoryItem item))
        {
            item.RemovefromStack();
            if (item.stackSize == 0)
            {
                inventory.Remove(item);
                itemDictionary.Remove(itemData);
            }
        }
        
    }
}
