using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]

//no need of mono behaviour 'cuz no attaching to game object
public class InventoryItem
{
    //data of item
    public ItemData itemData;
    //number of items in stack
    public int stackSize;

    public InventoryItem(ItemData item)
    {
        itemData = item;
        //method fot adding item
        AddtoStack();
    }

    public void AddtoStack()
    {
        //adding +1 to stack
        stackSize++;
    }

    public void RemovefromStack()
    {
        //removing -1 from stack
        stackSize--;
    }
}
