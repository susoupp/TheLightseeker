using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class ItemData : ScriptableObject
{
  //name of item
  public string displayName;
  //sprite for item's icon
  public Sprite icon;
}
