using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NBattery : MonoBehaviour
{
    public BatteryLive batteryLife;
    public float batteryAmount;

    public void OnTriggerEnter(Collider other)
    {
        Collect();
    }

    public void Collect()
    {
        Debug.Log("Battery, yea!");
        AddEnergy();
        Destroy(gameObject);
       
      
    }

    private void AddEnergy()
    {
        batteryLife.batteryLife += batteryAmount;
        Debug.Log( batteryLife.batteryLife);
    }
}
