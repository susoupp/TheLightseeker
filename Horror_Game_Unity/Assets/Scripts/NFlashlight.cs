using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NFlashlight : MonoBehaviour
{
    public Transform flashlightTransform;
    //public Transform playerTransform;
    public float detectionRange = 5f;
    public LayerMask enemyLayers;
    public float damage;
    public BatteryLive batteryScript;
    //public  EnemyLife enemies;
    
    
    
    

    

    void Update()
    {
        DetectObjectsInFlashlight();
        

    }

    void DetectObjectsInFlashlight()
    {

        Vector3 flashlightPosition = flashlightTransform.position;
        Vector3 flashlightForward = flashlightTransform.forward;
        Vector3 flashlightCenter = flashlightPosition + flashlightForward * detectionRange / 2f;
        
       // Vector3 playerPosition = playerTransform.position;
        

        Collider[] colliders = Physics.OverlapBox(flashlightCenter, new Vector3(detectionRange / 2f, 0.1f, detectionRange / 2f), flashlightTransform.rotation, enemyLayers);
       // Vector3 targetDir = playerPosition - transform.position;
        //float angle = Vector3.Angle(targetDir, transform.forward);
        //Debug.Log(angle);
        foreach (Collider collider in colliders)
        {
            Debug.Log(batteryScript.isFlashlightOn);
            if (batteryScript.isFlashlightOn == true)
            {
                collider.GetComponent<EnemyLife>().TakeDamage(damage);
           
                Debug.Log("Obiekt w zasięgu latarki: " + collider.gameObject.name);
            }
            
        }
       /* if (angle < 180f)
        {
            foreach (Collider collider in colliders)
            {

                Debug.Log("Obiekt w zasięgu latarki: " + collider.gameObject.name);
            }
        }*/
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(transform.position + transform.forward *  detectionRange* 0.5f, Vector3.one *  detectionRange );
    }

    
}
