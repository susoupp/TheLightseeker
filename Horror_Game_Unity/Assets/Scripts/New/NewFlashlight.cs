using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewFlashlight : MonoBehaviour
{
    [SerializeField] private GameObject flashlightLight;
    private bool flashlightActive = false;
    void Start()
    {
        flashlightLight.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (flashlightActive == false)
            {
                flashlightLight.gameObject.SetActive(true);
                flashlightActive = true;
            }
            else
            {
                flashlightLight.gameObject.SetActive(false);
                flashlightActive = false;
            }
        }
    }
}
