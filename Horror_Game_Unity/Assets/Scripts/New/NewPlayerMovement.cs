using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UIElements;

public class NewPlayerMovement : MonoBehaviour
{
   private Rigidbody rb;
   public Transform head;
   private Vector3 direction;
   public float playerAcceleration = 2.0f;
   public float runSpeed = 10.0f;
   public float jumpForce = 6f;
   public LayerMask groundLayer;
   public bool isRunning = false;
   private Vector3 crouchScale = new Vector3(1, 0.5f, 1);
   private Vector3 playerScale = new Vector3(1, 1f, 1);
   void Start()
   {
      rb = GetComponent<Rigidbody>();
   }

 
   void Update()
   {
      direction = Input.GetAxisRaw("Horizontal") * head.right + Input.GetAxisRaw("Vertical") * head.forward;
      rb.velocity = Vector3.Lerp(rb.velocity, direction.normalized * playerAcceleration
                                              + rb.velocity.y * Vector3.up, playerAcceleration * Time.deltaTime);
      if (Input.GetButtonDown("Jump") && isTouchingGround())
      {
         rb.velocity += jumpForce * Vector3.up;
      }

      Run();
      Crouch();


   }

   private bool isTouchingGround()
   {
      return Physics.CheckBox(transform.position, new Vector3(1f, 1f, 1f), Quaternion.identity, groundLayer);
   }

   private void Run()
   {
      if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
      {

         isRunning = true;
         direction = Input.GetAxisRaw("Horizontal") * head.right + Input.GetAxisRaw("Vertical") * head.forward;
         rb.velocity = Vector3.Lerp(rb.velocity, direction.normalized * runSpeed
                                                 + rb.velocity.y * Vector3.up, runSpeed * Time.deltaTime);
         Debug.Log("Running");
      }
      else
      {
         isRunning = false;
        

      }
   }

   private void Crouch()
   {
      if (Input.GetKeyDown(KeyCode.LeftControl))
      {

         transform.localScale = crouchScale;
         transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);

      } else if (Input.GetKeyUp(KeyCode.LeftControl)){ 
        
         transform.localScale = playerScale;
         transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f,  transform.position.z);
         
      } 
   }
   
}
