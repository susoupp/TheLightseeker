using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NoteHandler : MonoBehaviour
{
    //public ItemData notesData;
    public GameObject Note;
    private bool _isShow;

    private void Awake()
    {
        //showNote();
        //_isShow = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R)&& !_isShow)
        {
            showNote();
            _isShow = true;
        }
        else if (Input.GetKeyDown(KeyCode.R)&& _isShow)
       
        {
           hideNote();
           _isShow = false;
        }
        
    }

    public void showNote()
    {
        Note.SetActive(true);
        Time.timeScale = 0;
    }
    public void hideNote()
    {
        Note.SetActive(false);
        Time.timeScale = 1;
    }
}
