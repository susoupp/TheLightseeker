using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes : MonoBehaviour, ICollectible
{
    public static event HandleNoteCollector OnNoteCollected;
    public delegate void HandleNoteCollector(ItemData itemData);
    public ItemData notesData;
    
    
   
    public void Collect()
    {
        Debug.Log("Note?");
        Destroy(gameObject);
        OnNoteCollected?.Invoke(notesData);
        
    }
}
