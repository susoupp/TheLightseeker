using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    //private PlayerController playerContr;
    private Animator _anim;
    void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Anim();
    }
    public void Anim()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            _anim.SetBool("isDown" ,true);
            _anim.SetBool("isTop" ,false);
            _anim.SetBool("isRight" ,false);
           _anim.SetBool("isLeft" ,false);
           _anim.SetBool("isStop", false);
        }
        else if(Input.GetKeyDown(KeyCode.W))
        {
            _anim.SetBool("isDown" ,false);
            _anim.SetBool("isTop" ,true);
            _anim.SetBool("isRight" ,false);
            _anim.SetBool("isLeft" ,false);
            _anim.SetBool("isStop", false);
        }
        else if(Input.GetKeyDown(KeyCode.D))
        {
            _anim.SetBool("isRight" ,true);
            _anim.SetBool("isLeft" ,false);
            _anim.SetBool("isDown" ,false);
           _anim.SetBool("isTop" ,false);
           _anim.SetBool("isStop", false);
        }
        else if(Input.GetKeyDown(KeyCode.A))
        {
            _anim.SetBool("isLeft" ,true);
            _anim.SetBool("isRight" ,false);
            _anim.SetBool("isStop", false);
        }
        /*if(Input.GetKeyDown(KeyCode.W)&&Input.GetKeyDown(KeyCode.D)||Input.GetKeyDown(KeyCode.W)&&Input.GetKeyDown(KeyCode.A))
        {
            _anim.SetBool("isDown" ,false);
            _anim.SetBool("isTop" ,true);
            _anim.SetBool("isRight" ,false);
            _anim.SetBool("isLeft" ,false);
        }
        if(Input.GetKeyDown(KeyCode.S)&&Input.GetKeyDown(KeyCode.D)||Input.GetKeyDown(KeyCode.S)&&Input.GetKeyDown(KeyCode.A))
        {
            _anim.SetBool("isDown" ,true);
            _anim.SetBool("isTop" ,false);
            _anim.SetBool("isRight" ,false);
            _anim.SetBool("isLeft" ,false);
        }*/
        else if (!Input.anyKey)
        {
            _anim.SetBool("isStop", true);
            _anim.SetBool("isRight" ,false);
            _anim.SetBool("isLeft" ,false);
            _anim.SetBool("isDown" ,false);
            _anim.SetBool("isTop" ,false);
          
        }
        

    }
}

