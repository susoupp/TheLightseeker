using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
   
    [SerializeField] private Rigidbody _rb;
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private float _speed =5;
    private Vector3 _input;
    

  
    void Update()
    {
        //CollectInput();
        _input = transform.position;
    }
    void FixedUpdate()
    {
        Move();
        
    }
   // private void CollectInput()
   // {
       // _input= new Vector2(Input.GetAxisRaw("Horizontal"),Input.GetAxisRaw("Vertical")).normalized;
   // }
    private void Move()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");
        Vector3 _input = new Vector3(x, 0, y).normalized;
        _rb.velocity = _input * _speed;
    }

    
}


