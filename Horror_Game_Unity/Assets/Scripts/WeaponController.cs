using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject Knife;
    public bool CanAttack = true;
    public float AttackCooldown = 1.0f;
    public float damage = 10f;
    public EnemyLife enemy;
   

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (CanAttack = true)
            {
                KnifeAttack();
            }
        }
    }

    public void KnifeAttack()
    {
        CanAttack = false;
        Animator anim = Knife.GetComponent<Animator>();
        anim.SetTrigger("isAttacking");
        StartCoroutine(ResetAttackCooldown());
        enemy.TakeDamage(damage);
       
            

    }

    

    IEnumerator ResetAttackCooldown()
    {
        yield return new WaitForSeconds(AttackCooldown);
        CanAttack = true;
    }
}
