using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Image = UnityEngine.UI.Image;

public class aidKit : MonoBehaviour, ICollectible
{
    public static event HandleAidKitCollector OnAidKitCollected;
    public delegate void HandleAidKitCollector(ItemData itemData);
    public ItemData aidKitData;
    public GameEvent PlayerHealthChange;
    public PlayerStats healthAmount ;
    
   
    public void Collect()
    {
        Debug.Log("Aid kit, yea!");
        Destroy(gameObject);
        Heal(15f);
        OnAidKitCollected?.Invoke(aidKitData);
        informUI();
    }
    public void Heal(float healingAmount)
    {
        healthAmount.value += healingAmount;

    }
    public void informUI()
    {
        PlayerHealthChange.Raise(this, healthAmount);
    }
    
}
